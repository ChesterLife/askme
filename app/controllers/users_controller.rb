class UsersController < ApplicationController
  def index
    @users = [
        User.new(
            id: 1,
            name: 'Rinat',
            username: 'ChesterLife',
            avatar_url: 'https://s.gravatar.com/avatar/f109a9b2cb97061cbb2fb843e1bd2f5a?s=100'
        ),
        User.new(
            id: 2,
            name: 'John',
            username: 'SuperJohn'
        )
    ]
  end

  def new
  end

  def edit
  end

  def show
    @user = User.new(
            name: 'Rinat',
            username: 'ChesterLife',
            avatar_url: 'https://s.gravatar.com/avatar/f109a9b2cb97061cbb2fb843e1bd2f5a?s=100'
    )

    @questions = [
      Question.new(text: 'How are you?', created_at: Date.parse('27.03.2016')),
      Question.new(text: 'What you do?', created_at: Date.parse('27.03.2016'))
    ]

    @new_question = Question.new

  end
end
